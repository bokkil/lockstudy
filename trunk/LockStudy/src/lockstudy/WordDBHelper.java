package lockstudy;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WordDBHelper extends SQLiteOpenHelper{
	private static final int DATABASE_VERSION = 2;

	public WordDBHelper(Context context) {
		super(context, "voca.db", null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		int version = oldVersion;
		if (version != DATABASE_VERSION) {
			db.execSQL("ALTER TABLE userdata ADD COLUMN ans_eng INTEGER NOT NULL DEFAULT 0;");
			db.execSQL("ALTER TABLE userdata ADD COLUMN service_on INTEGER NOT NULL DEFAULT 0;");

		}
	}
}
