package lockstudy;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import bokkil.lockstudy.R;

public class SecondTab extends Activity implements OnItemClickListener{  // ListActivity 를 상속 받아야 ListView + 알파 가 가능하다.


	private ListView list;
	static final String[] m_item_string = new String[] {"1회차 20단어",
		"2회차 20단어",
		"3회차 20단어",
		"4회차 20단어",
		"5회차 20단어",
		"6회차 20단어",
		"7회차 20단어",
		"8회차 20단어",
		"9회차 20단어",
		"10회차 20단어",
		"11회차 20단어",
		"12회차 20단어",
		"13회차 20단어",
		"14회차 20단어",
		"15회차 20단어",
		"16회차 20단어",
		"17회차 20단어",
		"18회차 20단어",
		"19회차 20단어",
		"20회차 20단어",
		"21회차 20단어",
		"22회차 20단어",
		"23회차 20단어",
		"24회차 20단어",
		"25회차 20단어",
		"26회차 20단어",
		"27회차 20단어",
		"28회차 20단어",
		"29회차 20단어",
		"30회차 20단어",
		"31회차 20단어",
		"32회차 20단어",
		"33회차 20단어",
		"34회차 20단어",
		"35회차 20단어",
		"36회차 20단어",
		"37회차 20단어",
		"38회차 20단어",
		"39회차 20단어",
		"40회차 20단어",
		"41회차 20단어",
		"42회차 20단어",
		"43회차 20단어",
		"44회차 20단어",
		"45회차 20단어",
		"46회차 20단어",
		"47회차 20단어",
		"48회차 20단어",
		"49회차 20단어",
	"50회차 20단어"};


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.secondt);

		ListView list = (ListView)findViewById(R.id.id_list);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.stglist, m_item_string);

		list.setAdapter(adapter);
		list.setOnItemClickListener(this);

	}

	public void onItemClick(AdapterView<?> parent, View v, int position, long id)
	{
		// 두가지 방법 모두 사용가능하다.
		//	      Data data = (Data) parent.getItemAtPosition(position);
		//Data data = mList.get(position);

		// 다음 액티비티로 넘길 Bundle 데이터를 만든다.
		Bundle extras = new Bundle();
		//extras.putString("title", data.getTitle());
		//extras.putString("description", data.getDescription());
		extras.putLong("item_id", id);

		// 인텐트를 생성한다.
		// 컨텍스트로 현재 액티비티를, 생성할 액티비티로 ItemClickExampleNextActivity 를 지정한다.


		Log.i("Column", "id "+id+"   "+"Position ");
		Intent i = new Intent(this, ThirdTab.class);

		// 위에서 만든 Bundle을 인텐트에 넣는다.
		i.putExtras(extras);

		// 액티비티를 생성한다.
		startActivity(i);
	}
}





