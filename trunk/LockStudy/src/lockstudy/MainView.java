package lockstudy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import bokkil.lockstudy.R;

public class MainView extends Activity{
	/** Called when the activity is first created. */
	TextView answerText; //문제 텍스트뷰  
	Button btn1, btn2, btn3, btn4; //보기 1234 버튼 
	int xStart, xEnd; //드래그 시작과 끝 지점 
	String[] WORD ={"_id", "word", "ENG", "KOR", "AtoZ", "STG", "Correct", "Wrong", "LV"};
	String ATOZ, ANS_NAME, ANS, ANS_ID, WRONG, mSdPath, ext, STG;
	Random rand = new Random();
	int Correct, Wrong, Total, LV, service_On, ans_Eng, i;
	public Cursor user_c, c, c2; // 디비커서 
	private RelativeLayout layout;
	private LayoutParams params;
	NaviImageView nImageView;
	private float oldPosX = 0;
	private float oldPosY = 0;
	private Point curPoint = null;
	String usrdataupdate, serviceupdate;
	WindowManager wm;
	private Bitmap bitmap = null;
	ComponentName mService;    // 시작 서비스의 이름
	TextView mTextView;              // 서비스 상태 표시
	//DB옮기기
	public SQLiteDatabase db, db2;

	public static final String ROOT_DIR = "/data/data/bokkil.lockstudy/databases/";
	WordDBHelper mHelper;
	WordDBHelper2 upHelper;

	public int LastWords;

	int mState;
	int total;
	final static int STATE_DONE = 0;
	final static int STATE_RUNNING = 1;
	final static int PROGRESS_DIALOG = 1;

	ProgressDialog pDialog;

	private Handler handler = new Handler();


	public void setDB() {
		File folder = new File(ROOT_DIR);
		if(folder.exists()) {
			Log.i("File_Root_Dir", "  "+folder);
		}
		else {
			folder.mkdirs();
			//Toast.makeText(this, "폴더생성", Toast.LENGTH_LONG).show();
		}
		AssetManager assetManager = getResources().getAssets();
		File outfile = new File(ROOT_DIR+"voca.db"); //--폰에 위치할 경로
		File outfile2 = new File(ROOT_DIR+"voca2.db");

		InputStream is = null; 
		InputStream is2 = null;

		FileOutputStream fo = null;

		long filesize = 0;
		long filesize2 = 0;
		Log.i("FileSize", "  "+filesize);
		Log.i("FileSize2", "  "+filesize2);


		try {
			// --asset 폴더 및 복사할 DB 지정
			is = assetManager.open("voca.db", AssetManager.ACCESS_BUFFER);
			is2 = assetManager.open("voca2.db", AssetManager.ACCESS_BUFFER);
			filesize = is.available(); //--사이즈 검증
			filesize2 = is2.available(); //--사이즈 검증
			Log.i("FileSize", "  "+filesize);
			Log.i("FileSize2", "  "+filesize2);
			// 파일이 없거나 패키지 폴더에 설치된 DB파일이 포함된 DB파일 보다 크기가 같지않을 경우 DB파일을 덮어 쓴다.
			if (outfile.length() <= 4096) {
				byte[] tempdata = new byte[(int) filesize];
				is.read(tempdata); 
				is.close(); 
				outfile.createNewFile();
				fo = new FileOutputStream(outfile);
				fo.write(tempdata);
				fo.close();    
			}
			else
			{

				//				db=mHelper.getWritableDatabase();
				//				user_c=db.query("userdata", null, null, null, null, null, null);
				//				startManagingCursor(user_c);
				//				user_c.moveToFirst();
				//				LV = user_c.getInt(1);
				//				Total = user_c.getInt(2);
				//				Correct = user_c.getInt(4);
				//				Wrong = user_c.getInt(3);
				//
				//
				//				byte[] tempdata = new byte[(int) filesize];
				//				is.read(tempdata); 
				//				is.close(); 
				//				outfile.createNewFile();
				//				fo = new FileOutputStream(outfile);
				//				fo.write(tempdata);
				//				fo.close();
				//
				//
				//				usrdataupdate="UPDATE userdata SET usr_total="+Total+", usr_correct="+Correct+", usr_lv="+LV+", usr_wrong="+Wrong;
				//				db.execSQL(usrdataupdate);
				//				db.close();
				//Toast.makeText(this, "db있음", Toast.LENGTH_LONG).show();

			}
			if (outfile2.length() <= 4096) {
				byte[] tempdata = new byte[(int) filesize2];
				is2.read(tempdata); 
				is2.close(); 
				outfile2.createNewFile();
				fo = new FileOutputStream(outfile2);
				fo.write(tempdata);
				fo.close();    
			}
			else
			{

				//				db=mHelper.getWritableDatabase();
				//				user_c=db.query("userdata", null, null, null, null, null, null);
				//				startManagingCursor(user_c);
				//				user_c.moveToFirst();
				//				LV = user_c.getInt(1);
				//				Total = user_c.getInt(2);
				//				Correct = user_c.getInt(4);
				//				Wrong = user_c.getInt(3);
				//
				//
				//				byte[] tempdata = new byte[(int) filesize];
				//				is.read(tempdata); 
				//				is.close(); 
				//				outfile.createNewFile();
				//				fo = new FileOutputStream(outfile);
				//				fo.write(tempdata);
				//				fo.close();
				//
				//
				//				usrdataupdate="UPDATE userdata SET usr_total="+Total+", usr_correct="+Correct+", usr_lv="+LV+", usr_wrong="+Wrong;
				//				db.execSQL(usrdataupdate);
				//				db.close();
				//Toast.makeText(this, "db있음", Toast.LENGTH_LONG).show();

			}
		} catch (IOException e) { 
			Toast.makeText(this, "DB이동실패, 관련된 데이터를 모두 지우고 다시 설치해주세요", Toast.LENGTH_LONG).show();
		}   
	} 



	public void upDB(){
		db2=upHelper.getWritableDatabase();
		db=mHelper.getWritableDatabase();
		c2=db2.query("words2k", WORD, null, null, null, null, null);
		startManagingCursor(c2);
		c2.moveToFirst();
		Log.i("db_trans", c2.getString(1)+" "+c2.getString(2)+" "+c2.getString(3)+" "+c2.getString(4)+" "+c2.getInt(5)+" "+c2.getInt(6) );
		startActivity(new Intent(this, Splash.class));

		for(i=0;i<1000;i++){
			// 필요한 데이터를 입력합니다.
			ContentValues newValues = new ContentValues();
			LastWords = c2.getInt(0);
			newValues.put("word", c2.getString(1));
			newValues.put("ENG", c2.getString(2));
			newValues.put("KOR", c2.getString(3));
			newValues.put("AtoZ", c2.getString(4));
			newValues.put("STG", c2.getInt(5));
			// 레코드를 추가합니다.
			db.insert("words", null, newValues);
			c2.moveToNext();
			newValues.clear();
		}

		serviceupdate="UPDATE userdata SET service_on=0";
		db.execSQL(serviceupdate);
		c2.close();
		db2.close();
		db.close();
		Log.i("ser_on???", "     "+service_On);

	}


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER,
				WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER);
		setContentView(R.layout.copyoflockscreen);
		


		//실행 중인 서비스 확인
		ActivityManager am = (ActivityManager)this.getSystemService(ACTIVITY_SERVICE);
		List<ActivityManager.RunningServiceInfo> rs = am.getRunningServices(50);

		boolean serviceAlive = false;

		for(int i=0;i<rs.size(); i++){
			ActivityManager.RunningServiceInfo rsi = rs.get(i);
			Log.i("cur_service","Class Name : " + rsi.service.getClassName());

		}

		layout = (RelativeLayout) findViewById(R.id.Layout4);
		//if( km == null ) km = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
		//if( keyLock == null) keyLock = km.newKeyguardLock(KEYGUARD_SERVICE);		
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setAlpha(200);
		layout.setBackgroundColor(paint.getColor());
		nImageView = new NaviImageView(this);


		setDB();


		curPoint = new Point();
		mHelper= new WordDBHelper(this);
		upHelper= new WordDBHelper2(this);
		db=mHelper.getWritableDatabase();
		user_c=db.query("userdata", null, null, null, null, null, null);
		startManagingCursor(user_c);
		user_c.moveToFirst();
		service_On=user_c.getInt(6);
		ans_Eng=user_c.getInt(5);
		Log.i("user", ans_Eng+"  "+service_On+"       db    "+user_c.getInt(6));
		user_c.close();

		c=db.query("words", WORD, null, null, null, null, null);
		startManagingCursor(c);
		c.moveToLast();
		LastWords=c.getInt(0);
		Log.i("last", LastWords+"  "+c.getInt(0));
		c.close();
		db.close();

		answerText = (TextView) findViewById(R.id.txtQuest1);
		btn1 = (Button) findViewById(R.id.btnA11);
		btn1.setDrawingCacheEnabled(true);
		btn2 = (Button) findViewById(R.id.btnA21);
		btn2.setDrawingCacheEnabled(true);
		btn3 = (Button) findViewById(R.id.btnA31);
		btn3.setDrawingCacheEnabled(true);
		btn4 = (Button) findViewById(R.id.btnA41);
		btn4.setDrawingCacheEnabled(true);

		Log.i("ser_on?", "     "+service_On);
		String[] word = new String[4];

		ANS = "여기로 원하는 메뉴를\n끌어다 놓으세요.";
		answerText.setText(ANS);
		answerText.setSelected(true);
		answerText.setClickable(true);
		//		answerText.setOnTouchListener(new OnTouchListener() {
		//
		//			public boolean onTouch(View v, MotionEvent event) {
		//				// TODO Auto-generated method stub
		//				if (event.getAction() == MotionEvent.ACTION_DOWN) {
		//					xStart = (int) event.getX();
		//				}
		//				if (event.getAction() == MotionEvent.ACTION_UP) {
		//					xEnd = (int) event.getX();
		//					if ((xEnd+200) < xStart) {
		//						customToastShow("unlock!");
		//						finish();
		//					} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
		//					}
		//				}
		//				return false;
		//			}
		//		});

		Animation a = new TranslateAnimation(-100, 0, 0, 0);
		a.setDuration(500);
		a.setInterpolator(AnimationUtils.loadInterpolator(this,
				android.R.anim.overshoot_interpolator));
		Animation b = new TranslateAnimation(100, 0, 0, 0);
		b.setDuration(500);
		b.setInterpolator(AnimationUtils.loadInterpolator(this,
				android.R.anim.overshoot_interpolator));


		word[0]="마이 페이지";
		word[1]="도움말";
		word[2]="잠금화면 등록";
		word[3]="잠금화면 해제";
		btn1.setText(word[0]);
		btn2.setText(word[1]);
		if(service_On==0){
			btn3.setText(word[2]);
		}else{
			btn3.setText(word[3]);
		}
		btn4.setText("문제 풀기");
		btn1.setBackgroundResource(R.drawable.circle);
		btn2.setBackgroundResource(R.drawable.circle);
		btn3.setBackgroundResource(R.drawable.circle);
		btn4.setBackgroundResource(R.drawable.circle);
		btn1.setAnimation(a);
		btn2.setAnimation(b);
		btn3.setAnimation(a);
		btn4.setAnimation(b);
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		final Intent act1 = new Intent(this, MyPage.class);

		View.OnTouchListener d1 = new OnTouchListener() {


			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap1(btn1.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn1.setVisibility(view.INVISIBLE);
					answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn1.setVisibility(view.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);
					answerText.setBackgroundColor(Color.TRANSPARENT);

					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){
						startActivity(act1);	
					}

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}
		};
		View.OnTouchListener d2 = new OnTouchListener() {


			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap2(btn2.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn2.setVisibility(view.INVISIBLE);
					answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn2.setVisibility(view.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);
					answerText.setBackgroundColor(Color.TRANSPARENT);
					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){
						customToastShow2("1. 원하는 답이나 메뉴를\n2. 잡아 끌어서\n3. 아래의 네모진 영역에 놓으세요\n4. 끗\n만든 사람들 : Wisdom Hub\n엄준성 ujs84711@gmail.com\n송의정 dieoxenh@korea.ac.kr\n조윤하 bahrain777@naver.com");
					}

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}
		};
		View.OnTouchListener d3 = new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap3(btn3.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn3.setVisibility(view.INVISIBLE);
					answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn3.setVisibility(view.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);
					answerText.setBackgroundColor(Color.TRANSPARENT);

					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){

						//	keyLock.disableKeyguard();
						db=mHelper.getWritableDatabase();
						user_c=db.query("userdata", null, null, null, null, null, null);
						startManagingCursor(user_c);
						user_c.moveToFirst();
						if(service_On==0){
							serviceupdate="UPDATE userdata SET service_on=1";
							db.execSQL(serviceupdate);
							customToastShow("잠금화면에 등록되었습니다!!");
							Log.i("ser_on?", "     "+service_On);
							startHelloService();
							Intent i = new Intent(getApplicationContext(), NewLockStudyActivity.class);
							startActivity(i);

						}
						else{
							serviceupdate="UPDATE userdata SET service_on=0";
							db.execSQL(serviceupdate);
							Log.i("ser_on???????", "     "+service_On+"     db   "+user_c.getInt(6));
							customToastShow("잠금화면 등록이 해지되었습니다!!");
							stopHelloService();
						}
						db.close();
						finish();

					}

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}
		};
		View.OnTouchListener d4 = new OnTouchListener() {

			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap4(btn4.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn4.setVisibility(view.INVISIBLE);
					answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn4.setVisibility(view.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);
					answerText.setBackgroundColor(Color.TRANSPARENT);
					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){
						Intent i = new Intent(getApplicationContext(), StudyActivity.class);
						startActivity(i);
						finish();
						/*	km.exitKeyguardSecurely(new OnKeyguardExitResult() {
								public void onKeyguardExitResult(boolean success) {
								keyLock.reenableKeyguard();
								}
							});*/
					}

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}
		};

		btn1.setOnTouchListener(d1);
		btn2.setOnTouchListener(d2);
		btn3.setOnTouchListener(d3);
		btn4.setOnTouchListener(d4);


		if(LastWords<=1500){


			startActivity(new Intent(this, Splash.class));

			

		}




		//Resources res = getResources(); // Resource object to get Drawables // 탭 아이콘
		//		TabHost tabHost = getTabHost();  // The activity TabHost
		//		TabHost.TabSpec spec;  // Resusable TabSpec for each tab
		//		Intent intent;  // Reusable Intent for each tab
		//
		//		// Create an Intent to launch an Activity for the tab (to be reused)
		//		intent = new Intent().setClass(this, FisrtTab.class);
		//
		//		// Initialize a TabSpec for each tab and add it to the TabHost
		//		spec = tabHost.newTabSpec("artists").setIndicator("Setting")  // 탭 아이콘 
		//				.setContent(intent);
		//		tabHost.addTab(spec);
		//
		//		// Do the same for the other tabs
		//		intent = new Intent().setClass(this, SecondTab.class);
		//		spec = tabHost.newTabSpec("albums").setIndicator("Word List")  // 탭 아이콘 
		//				.setContent(intent);
		//		tabHost.addTab(spec);
		//
		//		intent = new Intent().setClass(this, CopyOfThirdTab.class);
		//		spec = tabHost.newTabSpec("songs").setIndicator("My Page")  // 탭 아이콘 
		//				.setContent(intent);
		//		tabHost.addTab(spec);
		//
		//		tabHost.setCurrentTab(0);
	}

	class NaviImageView extends View {


		public NaviImageView(Context context) {
			super(context);

			setFocusable(true);

			//curPoint.x = 0;
			//curPoint.y = 0;
			// TODO Auto-generated constructor stub
		}

		public NaviImageView(Context context, AttributeSet attrs) {
			super(context, attrs);
			// TODO Auto-generated constructor stub
		}

		public NaviImageView(Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
			// TODO Auto-generated constructor stub
		}

		public void setBitmap1(Bitmap context) {

			Bitmap bitmapOrg = btn1.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		public void setBitmap2(Bitmap context) {

			Bitmap bitmapOrg = btn2.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		public void setBitmap3(Bitmap context) {

			Bitmap bitmapOrg = btn3.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		public void setBitmap4(Bitmap context) {

			Bitmap bitmapOrg = btn4.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		@Override
		protected void onDraw(Canvas canvas) {
			canvas.drawBitmap(bitmap, curPoint.x, curPoint.y, null);
		}

		public void setNewPosition(int newPosX, int newPosY) {
			curPoint.x += newPosX;
			curPoint.y += newPosY;
			Log.i("hsw_test", "Point has changed to x:["+ curPoint.x + "]" + "y:[" + curPoint.y + "]");
			invalidate();
		}

	}
	// 서비스 시작 요청
	private void startHelloService() {
		mService = startService(new Intent(this, AutoRegisterFilter.class));
	}

	// 실행한 서비스 중지 요청
	private void stopHelloService() {
		//Intent i = new Intent();
		//i.setComponent(mService);
		//stopService(i);
		stopService(new Intent(this, AutoRegisterFilter.class));
	}

	private void clearApplicationCache(java.io.File dir){
		if(dir==null)
			dir = getCacheDir();
		else;
		if(dir==null)
			return;
		else;
		java.io.File[] children = dir.listFiles();
		try{
			for(int i=0;i<children.length;i++)
				if(children[i].isDirectory())
					clearApplicationCache(children[i]);
				else children[i].delete();
		}
		catch(Exception e){}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		clearApplicationCache(null);
	} 
	//커스터마이징된 토스트를 띄우는 부분
	public void customToastShow(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE 그림
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_SHORT); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}
	public void customToastShow2(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE 그림
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_LONG); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}


	public class WordDBHelper2 extends SQLiteOpenHelper{
		private static final int DATABASE_VERSION = 1;

		public WordDBHelper2(Context context) {
			super(context, "voca2.db", null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub

		}

		@Override
		public void onCreate(SQLiteDatabase db2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
		}
	}


	// GUI Handler 처리..


	// 이 메서드는 메인 GUI 스레드에서 호출된다.
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case PROGRESS_DIALOG:
			pDialog = new ProgressDialog(MainView.this);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.setMax(2000);
			pDialog.show();
			Thread thread = new Thread(null, doBackgroundThreadProcessing, "Background");
			thread.start();
			return pDialog;
		default:
			return null;
		}
	}

	// 백그라운드 처리 메서드를 실행하는 Runnable
	private Runnable doBackgroundThreadProcessing = new Runnable(){
		public void run() {
			backgroundThreadProcessing();

		}
	};

	// 백그라운드에서 몇 가지 처리를 수행하는 메서드
	private void backgroundThreadProcessing(){
		//[필요한 코드] 
		// 처리가 끝나고 결과를 UI로 출력을 해야 할 때 아래 핸들러를 추가해서 사용한다.
		handler.post(doUpdateGUI);  
		upDB();
	}

	// GUI 업데이트 메서드를 실행하는 Runnable.
	private Runnable doUpdateGUI = new Runnable(){
		public void run() {
			mState = STATE_RUNNING;



			while (mState == STATE_RUNNING) {
				try {
					Thread.sleep(200);
				} catch (Exception e) {

				}
				Log.i("last", "      "+LastWords+"  ");
				pDialog.setProgress(LastWords);
				if(LastWords ==2000){
					mState = STATE_DONE;
					updateGUI();
				}

			}
		}

		private void updateGUI() {
			// [[ 필요한 UI 코드 ]] 
			customToastShow("update complete!!");
			pDialog.dismiss();
			dismissDialog(PROGRESS_DIALOG);
			removeDialog(PROGRESS_DIALOG);

		}

	};



}