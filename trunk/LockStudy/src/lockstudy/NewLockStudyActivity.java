package lockstudy;

import java.util.Random;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import bokkil.lockstudy.R;


public class NewLockStudyActivity extends Activity {
	/** Called when the activity is first created. */
	TextView answerText, usrStatus; //문제 텍스트뷰  
	Button btn1, btn2, btn3, btn4; //보기 1234 버튼 
	public SQLiteDatabase db; //디비 
	public Cursor cursor, rec_c, user_c; // 디비커서 
	WordDBHelper mHelper; //디비아답터 
	int xStart, xEnd; //드래그 시작과 끝 지점 
	int numAns; // 해답 번호 
	int total; // 단어 총 수 
	int usrCorrect, usrWrong, usrTotal, usrLV, rec_id, wordWrong, service_On;
	float usrSTG;
	String[] WORD ={"_id", "word", "ENG", "KOR", "AtoZ", "STG", "Correct", "Wrong", "LV"};
	String ATOZ, ANS_NAME, ANS, ANS_ID, WRONG, mSdPath, ext, STG,usrdataupdate, recentupdate, questupdate, status;
	int[] RECENT = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	int[] SEND = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
	Random rand = new Random();
	private RelativeLayout layout;
	private LayoutParams params;
	NaviImageView nImageView;
	private float oldPosX = 0;
	private float oldPosY = 0;
	private Point curPoint = null;
	WindowManager wm;
	private Bitmap bitmap = null;
	float perc, Total, Correct, Wrong;
	String[] word = new String[4];
	String[] wordmeaning = new String[4];

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		NotificationManager nmng = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nmng.cancelAll();
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER,
				WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.lockscreen);
		layout = (RelativeLayout) findViewById(R.id.Layout2);
		/*getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				|	WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
				WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
				);*/
		//KeyguardManager km = (KeyguardManager)getSystemService(KEYGUARD_SERVICE);
		//KeyguardManager.KeyguardLock keyLock = km.newKeyguardLock(KEYGUARD_SERVICE);
		//keyLock.disableKeyguard(); //원래 락스크린 해제
		nImageView = new NaviImageView(this);
		curPoint = new Point();

		//DB 세팅 
		mHelper=new WordDBHelper(this);

		answerText = (TextView) findViewById(R.id.txtQuest);
		usrStatus = (TextView) findViewById(R.id.usrStatus);
		btn1 = (Button) findViewById(R.id.btnA1);
		btn1.setDrawingCacheEnabled(true);
		btn2 = (Button) findViewById(R.id.btnA2);
		btn2.setDrawingCacheEnabled(true);
		btn3 = (Button) findViewById(R.id.btnA3);
		btn3.setDrawingCacheEnabled(true);
		btn4 = (Button) findViewById(R.id.btnA4);
		btn4.setDrawingCacheEnabled(true);


		
		WRONG = new String("*");

		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setAlpha(150);
		layout.setBackgroundColor(paint.getColor());

		numAns=rand.nextInt(4);
		Log.i("Number Answer", "  "+numAns);

		//사용자의 레벨에 따라 제시될 단어의 갯수를 설정. (테스트 레벨3=60단어)
		//db=mHelper.getReadableDatabase();
		db=mHelper.getWritableDatabase();






		user_c=db.query("userdata", null, null, null, null, null, null);
		startManagingCursor(user_c);
		user_c.moveToFirst();
		usrLV = user_c.getInt(1);

		Total = user_c.getInt(2);
		Correct = user_c.getInt(4);
		Wrong = user_c.getInt(3);
		perc = (Correct/Total)*100;
		service_On=user_c.getInt(6);
		
		if( Total > (usrLV*50) && perc > 70){
			usrLV++;
			if(usrLV>40){
				usrLV=40;
			}
			usrdataupdate="UPDATE userdata SET usr_lv="+usrLV;
			db=mHelper.getWritableDatabase();
			db.execSQL(usrdataupdate);
		}

		usrSTG = usrLV;
		Log.i("STG", "  "+usrSTG);
		STG = "STG <= "+usrSTG;
		Log.i("STG", "  "+STG);
		//유저 정답률 불러오기
		usrTotal = user_c.getInt(2);
		usrCorrect = user_c.getInt(4);
		usrWrong = user_c.getInt(3);
		user_c.close();
		//랜덤으로 4개의 보기를 정함
		cursor=db.query("words", WORD, STG, null, null, null, null, null);
		startManagingCursor(cursor);
		total = cursor.getCount();
		int[] list = new int[total];
		for (int i=0; i < total; i++){
			list[i]=i;
		}

		int idx;
		int nRandRange = total;
		Log.i("rand", "  "+nRandRange+"  "+total+"  "+list[0]+"  "+list[2]);
		for (int i = 0; i < 4; i++)
		{

			idx = rand.nextInt(nRandRange);
			int ex = list[idx];
			list[idx] = list[--nRandRange];
			cursor.moveToFirst();
			cursor.moveToPosition(ex);
			word[i] = cursor.getString(1);
			wordmeaning[i] = cursor.getString(3);
			Log.i("word"+i, "  "+word[i]);
			if (i == numAns){
				ANS = cursor.getString(3);
				ANS_NAME = cursor.getString(1);
				ANS_ID = cursor.getString(0);
				wordWrong = cursor.getInt(7);
			}
			else{
			}
			Log.i("Column"+i, "line "+total+" "+ex+" string "+word[i]+"  "+nRandRange);

		}
		Log.i("answer", "  "+ANS+"  "+ANS_NAME+"  "+ANS_ID);
		cursor.close();
		db.close();



		//정답이 제시 될 텍스트뷰의 설정과 비상탈출 기능의 터치리스너 설정
		answerText.setText(ANS);
		answerText.setSelected(true);
		answerText.setClickable(true);

		if(0<=perc&&perc<=100){
			status = usrLV+" Level, "+usrLV*50+"/2000 Words, "+(int)(perc)+"% Correct.";
		}
		else{
			status = usrLV+" Level, "+usrLV*50+"/2000 Words.";
		}
		usrStatus.setText(status);
		//		answerText.setOnTouchListener(new OnTouchListener() {
		//
		//			public boolean onTouch(View v, MotionEvent event) {
		//				// TODO Auto-generated method stub
		//				if (event.getAction() == MotionEvent.ACTION_DOWN) {
		//					xStart = (int) event.getX();
		//				}
		//				if (event.getAction() == MotionEvent.ACTION_UP) {
		//					xEnd = (int) event.getX();
		//					if ((xEnd+200) < xStart) {
		//						Toast.makeText(getApplicationContext(), "Unlock!!", Toast.LENGTH_SHORT).show();
		//						finish();
		//					} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
		//					}
		//				}
		//				return false;
		//			}
		//		});

		//보기들의 두 가지 애니메이션
		Animation a = new TranslateAnimation(-100, 0, 0, 0);
		a.setDuration(500);
		a.setInterpolator(AnimationUtils.loadInterpolator(this,
				android.R.anim.overshoot_interpolator));
		//a.setFillAfter(true);
		Animation b = new TranslateAnimation(100, 0, 0, 0);
		b.setDuration(500);
		b.setInterpolator(AnimationUtils.loadInterpolator(this,
				android.R.anim.overshoot_interpolator));
		//b.setFillAfter(true);
		//answerText.startAnimation(a);



		btn1.setText(word[0]);
		btn2.setText(word[1]);
		btn3.setText(word[2]);
		btn4.setText(word[3]);
		btn1.setBackgroundResource(R.drawable.circle);
		btn2.setBackgroundResource(R.drawable.circle);
		btn3.setBackgroundResource(R.drawable.circle);
		btn4.setBackgroundResource(R.drawable.circle);
		btn1.setAnimation(a);
		btn2.setAnimation(b);
		btn3.setAnimation(a);
		btn4.setAnimation(b);


		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);

		Log.i("ser_on?", "     "+service_On);



		//보기 4개의 터치리스너 설정 (최적화 필요)
		View.OnTouchListener d1 = new OnTouchListener() {
			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap1(btn1.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn1.setVisibility(View.INVISIBLE);
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn1.setVisibility(View.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);

					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){

						if(0==numAns) {

							customToastShow("Correct!!");

							usrTotal++;
							usrCorrect++;
							usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_correct="+usrCorrect;
							db=mHelper.getWritableDatabase();
							db.execSQL(usrdataupdate);
							db.close();
							clearApplicationCache(getCacheDir());
							finish();
							onDestroy();
						}else{customToastShow2("틀렸습니다\n고르신 단어 "+word[0]+"의 뜻은\n"+wordmeaning[0]+"\n입니다\n정답은 "+ANS_NAME+"입니다");
						db=mHelper.getWritableDatabase();
						rec_c=db.query("recent", null, null, null, null, null, null);
						startManagingCursor(rec_c);
						for(int l=18; l>=0; l--){
							rec_c.moveToPosition(l);
							rec_id=rec_c.getInt(1);
							recentupdate="UPDATE recent SET id="+rec_id+" WHERE _id="+(l+2);
							db.execSQL(recentupdate);

						}
						rec_c.close();
						usrTotal++;
						usrWrong++;
						recentupdate="UPDATE recent SET id="+ANS_ID+" WHERE _id=1";
						usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_wrong="+usrWrong;
						wordWrong++;
						questupdate="UPDATE words SET Wrong="+wordWrong+" WHERE _id="+ANS_ID;
						db=mHelper.getWritableDatabase();
						db.execSQL(usrdataupdate);
						db.execSQL(recentupdate);
						db.execSQL(questupdate);
						db.close();
						finish();
						}
					}
				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){
						answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					}
					else{
						answerText.setBackgroundColor(Color.TRANSPARENT);
					}
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}

		};
		View.OnTouchListener d2 = new OnTouchListener() {


			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap2(btn2.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn2.setVisibility(View.INVISIBLE);
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn2.setVisibility(View.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);
					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){

						if(1==numAns) {

							usrTotal++;
							usrCorrect++;
							usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_correct="+usrCorrect;
							db=mHelper.getWritableDatabase();
							db.execSQL(usrdataupdate);
							db.close();
							customToastShow("Correct!!");
							clearApplicationCache(getCacheDir());
							finish();
							onDestroy();

						}else{customToastShow2("틀렸습니다\n고르신 단어 "+word[1]+"의 뜻은\n"+wordmeaning[1]+"\n입니다\n정답은 "+ANS_NAME+"입니다");
						db=mHelper.getWritableDatabase();
						rec_c=db.query("recent", null, null, null, null, null, null);
						startManagingCursor(rec_c);
						for(int l=18; l>=0; l--){
							rec_c.moveToPosition(l);
							rec_id=rec_c.getInt(1);
							recentupdate="UPDATE recent SET id="+rec_id+" WHERE _id="+(l+2);
							db.execSQL(recentupdate);

						}
						rec_c.close();
						usrTotal++;
						usrWrong++;
						recentupdate="UPDATE recent SET id="+ANS_ID+" WHERE _id=1";
						usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_wrong="+usrWrong;
						wordWrong++;
						questupdate="UPDATE words SET Wrong="+wordWrong+" WHERE _id="+ANS_ID;
						db=mHelper.getWritableDatabase();
						db.execSQL(usrdataupdate);
						db.execSQL(recentupdate);
						db.execSQL(questupdate);
						db.close();
						finish();
						}
					}



				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){
						answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					}
					else{
						answerText.setBackgroundColor(Color.TRANSPARENT);
					}
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}
		};
		View.OnTouchListener d3 = new OnTouchListener() {


			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap3(btn3.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn3.setVisibility(View.INVISIBLE);
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn3.setVisibility(View.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);

					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){

						if(2==numAns) {

							usrTotal++;
							usrCorrect++;
							usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_correct="+usrCorrect;
							db=mHelper.getWritableDatabase();
							db.execSQL(usrdataupdate);
							db.close();
							customToastShow("Correct!!");
							clearApplicationCache(getCacheDir());
							finish();
							onDestroy();
						}else{customToastShow2("틀렸습니다\n고르신 단어 "+word[2]+"의 뜻은\n"+wordmeaning[2]+"\n입니다\n정답은 "+ANS_NAME+"입니다");
						db=mHelper.getWritableDatabase();
						rec_c=db.query("recent", null, null, null, null, null, null);
						startManagingCursor(rec_c);
						for(int l=18; l>=0; l--){
							rec_c.moveToPosition(l);
							rec_id=rec_c.getInt(1);
							recentupdate="UPDATE recent SET id="+rec_id+" WHERE _id="+(l+2);
							db.execSQL(recentupdate);

						}
						rec_c.close();
						usrTotal++;
						usrWrong++;
						recentupdate="UPDATE recent SET id="+ANS_ID+" WHERE _id=1";
						usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_wrong="+usrWrong;
						wordWrong++;
						questupdate="UPDATE words SET Wrong="+wordWrong+" WHERE _id="+ANS_ID;
						db=mHelper.getWritableDatabase();
						db.execSQL(usrdataupdate);
						db.execSQL(recentupdate);
						db.execSQL(questupdate);
						db.close();
						finish();
						}
					}

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){
						answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					}
					else{
						answerText.setBackgroundColor(Color.TRANSPARENT);
					}
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}
		};
		View.OnTouchListener d4 = new OnTouchListener() {


			public boolean onTouch(View view, MotionEvent me) {

				// TODO Auto-generated method stub
				nImageView.setBitmap4(btn4.getDrawingCache());

				if (me.getAction() == MotionEvent.ACTION_DOWN) {
					layout.addView(nImageView, params);
					btn4.setVisibility(View.INVISIBLE);
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();
					curPoint.x = (int) oldPosX-130;
					curPoint.y = (int) oldPosY-150;	
					Log.i("Column", "oldPosX "+oldPosX+"   "+"oldPosY"+oldPosY);
				}
				if (me.getAction() == MotionEvent.ACTION_UP) {
					Log.i("Drag", "Stopped Dragging");
					btn4.setVisibility(View.VISIBLE);
					nImageView.invalidate();
					layout.removeView(nImageView);

					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){

						if(3==numAns) {

							usrTotal++;
							usrCorrect++;
							usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_correct="+usrCorrect;
							db=mHelper.getWritableDatabase();
							db.execSQL(usrdataupdate);
							db.close();
							customToastShow("Correct!!");
							clearApplicationCache(getCacheDir());
							finish();
							onDestroy();
						}else{customToastShow2("틀렸습니다\n고르신 단어 "+word[3]+"의 뜻은\n"+wordmeaning[3]+"\n입니다\n정답은 "+ANS_NAME+"입니다");
						db=mHelper.getWritableDatabase();
						rec_c=db.query("recent", null, null, null, null, null, null);
						startManagingCursor(rec_c);
						for(int l=18; l>=0; l--){
							rec_c.moveToPosition(l);
							rec_id=rec_c.getInt(1);
							recentupdate="UPDATE recent SET id="+rec_id+" WHERE _id="+(l+2);
							db.execSQL(recentupdate);

						}
						rec_c.close();
						usrTotal++;
						usrWrong++;
						recentupdate="UPDATE recent SET id="+ANS_ID+" WHERE _id=1";
						usrdataupdate="UPDATE userdata SET usr_total="+usrTotal+", usr_wrong="+usrWrong;
						wordWrong++;
						questupdate="UPDATE words SET Wrong="+wordWrong+" WHERE _id="+ANS_ID;
						db=mHelper.getWritableDatabase();
						db.execSQL(usrdataupdate);
						db.execSQL(recentupdate);
						db.execSQL(questupdate);
						db.close();
						finish();
						}
					}

				} else if (me.getAction() == MotionEvent.ACTION_MOVE) {
					if((int)answerText.getLeft()<(int)me.getRawX()&&(int)me.getRawX()<(int)answerText.getRight()&&(int)answerText.getTop()<(int)me.getRawY()&&(int)me.getRawY()<(int)answerText.getBottom()){
						answerText.setBackgroundColor(Color.argb(50, 00, 00, 255));
					}
					else{
						answerText.setBackgroundColor(Color.TRANSPARENT);
					}
					nImageView.setNewPosition((int) (me.getRawX() - oldPosX), (int) (me.getRawY() - oldPosY));
					oldPosX = (int)me.getRawX();
					oldPosY = (int)me.getRawY();

				}
				return false;
			}
		};

		/*View.OnTouchListener t = new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					// 터치 시작지점 x좌표 저장
					xStart = (int) event.getX();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					// 터치 끝난지점 x좌표 저장
					xEnd = (int) event.getX();
					if ((xEnd+50) < xStart) {
						Toast.makeText(getApplicationContext(), "Unlock!!", Toast.LENGTH_SHORT).show();
						finish();
					} else if (xEnd > (xStart+50)) {
						int index = 0;

						switch (v.getId()) {
						case R.id.btnA4:
							index++;
						case R.id.btnA3:
							index++;
						case R.id.btnA2:
							index++;
						case R.id.btnA1:
							break;
						}

						if(index==numAns) {
							Toast.makeText(getApplicationContext(), "Thanks!!", Toast.LENGTH_SHORT).show();
							try {
								//ContentValues value = new ContentValues();
								//value.put("O", words.)
								//cursor=db.query("_iD", WORD, ANS_ID, null, null, null, null, null);
								//db.update("words", value, whereClause, whereArgs)

								BufferedWriter out = new BufferedWriter(new FileWriter(mSdPath+"lockstudy.txt", true));
								out.write(ANS_ID+","+ANS_NAME);
								out.newLine();
								out.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Intent i = new Intent(NewLockStudyActivity.this, NewLockStudyActivity.class);
							i.addFlags(i.FLAG_ACTIVITY_SINGLE_TOP);
							startActivity(i);
							finish();
						}else{Toast.makeText(getApplicationContext(), "Wrong Answer!!", Toast.LENGTH_SHORT).show();
						try {
							BufferedWriter out = new BufferedWriter(new FileWriter(mSdPath+"lockstudy.txt", true));
							out.write(WRONG);
							out.write(ANS_NAME);
							out.newLine();
							out.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Intent i = new Intent(NewLockStudyActivity.this, NewLockStudyActivity.class);
						startActivity(i);
						finish();
						}
					}
				}
				return true;
			}
		};*/
		btn1.setOnTouchListener(d1);
		btn2.setOnTouchListener(d2);
		btn3.setOnTouchListener(d3);
		btn4.setOnTouchListener(d4);

	}
	//커스터마이징된 토스트를 띄우는 부분
	public void customToastShow(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE 그림
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_SHORT); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}

	public void customToastShow2(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE 그림
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_LONG); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		return super.onTouchEvent(event);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return false;
	}
	@Override
	public void onAttachedToWindow()
	{  
		super.onAttachedToWindow(); 
		this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);     
	}
	@Override
	public void onBackPressed() {
	}

	class NaviImageView extends View {


		public NaviImageView(Context context) {
			super(context);

			setFocusable(true);

			//curPoint.y = 0;
			// TODO Auto-generated constructor stub
		}

		public NaviImageView(Context context, AttributeSet attrs) {
			super(context, attrs);
			// TODO Auto-generated constructor stub
		}

		public NaviImageView(Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
			// TODO Auto-generated constructor stub
		}

		public void setBitmap1(Bitmap context) {

			Bitmap bitmapOrg = btn1.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		public void setBitmap2(Bitmap context) {

			Bitmap bitmapOrg = btn2.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		public void setBitmap3(Bitmap context) {

			Bitmap bitmapOrg = btn3.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		public void setBitmap4(Bitmap context) {

			Bitmap bitmapOrg = btn4.getDrawingCache(); 
			int width = bitmapOrg.getWidth();
			int height = bitmapOrg.getHeight();

			Matrix matrix = new Matrix();
			bitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		}
		@Override
		protected void onDraw(Canvas canvas) {
			canvas.drawBitmap(bitmap, curPoint.x, curPoint.y, null);
		}

		public void setNewPosition(int newPosX, int newPosY) {
			curPoint.x += newPosX;
			curPoint.y += newPosY;
			Log.i("hsw_test", "Point has changed to x:["+ curPoint.x + "]" + "y:[" + curPoint.y + "]");
			invalidate();
		}

	}
	private void clearApplicationCache(java.io.File dir){
		if(dir==null)
			dir = getCacheDir();
		else;
		if(dir==null)
			return;
		else;
		java.io.File[] children = dir.listFiles();
		try{
			for(int i=0;i<children.length;i++)
				if(children[i].isDirectory())
					clearApplicationCache(children[i]);
				else children[i].delete();
		}
		catch(Exception e){}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		clearApplicationCache(null);
	} 

}
