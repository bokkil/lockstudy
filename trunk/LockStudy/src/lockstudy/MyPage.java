package lockstudy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import bokkil.lockstudy.R;

public class MyPage extends Activity {

	TextView usrTotalTV, usrCorrectTV, usrWrongTV, usrLVTV, Percent;
	public SQLiteDatabase db; //디비세팅 
	public Cursor cursor, rec_c, user_c; // µðºñÄ¿¼­ 
	WordDBHelper mHelper; //µðºñ¾Æ´äÅÍ 
	float perc, Total, Correct, Wrong;
	public SimpleCursorAdapter Adapter = null;
	public static final String TABLE_NAME ="words";
	String[] WORD ={"_id", "word", "ENG", "KOR", "AtoZ", "STG", "Correct", "Wrong", "LV"};
	String[] RECID ={"_id", "id"};
	String Lv, rawrct;
	long item_id;
	LinearLayout layout;

	String[] FQ_Wrong = new String[] {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0" };
	String[] FQ_Wrong2 = new String[] {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0" };
	String[] RCT = new String[] {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0" };
	String[] RCT2 = new String[] {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0" };
	int [] recent = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER,
				WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER);
		setContentView(R.layout.mypage);
		//GV = (GlobalV)getApplicationContext();

		layout = (LinearLayout) findViewById(R.id.mypage);

		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setAlpha(200);
		layout.setBackgroundColor(paint.getColor());


		mHelper=new WordDBHelper(this);
		for(int l=19; l>=0; l--){
			RCT[l]="µ¥ÀÌÅÍ°¡ ¾ÆÁ÷ ¾ø½À´Ï´Ù";
		}

		for(int l=19; l>=0; l--){
			FQ_Wrong[l]="µ¥ÀÌÅÍ°¡ ¾ÆÁ÷ ¾ø½À´Ï´Ù";
		}

		usrTotalTV = (TextView) findViewById(R.id.totaltv);
		usrCorrectTV = (TextView) findViewById(R.id.correcttv);
		usrWrongTV = (TextView) findViewById(R.id.wrongtv);
		usrLVTV = (TextView) findViewById(R.id.lvtv);
		Percent = (TextView) findViewById(R.id.percent);

		db=mHelper.getReadableDatabase();
		user_c=db.query("userdata", null, null, null, null, null, null);
		startManagingCursor(user_c);
		user_c.moveToFirst();
		usrTotalTV.setText("ÃÑ "+user_c.getString(2)+"¹®Á¦¸¦ Ç®¾ú½À´Ï´Ù");
		usrCorrectTV.setText(user_c.getString(4)+" ¹®Á¦¸¦ ¸ÂÃß¾ú½À´Ï´Ù");
		usrWrongTV.setText(user_c.getString(3)+" ¹®Á¦¸¦ Æ²·È½À´Ï´Ù");
		usrLVTV.setText("ÇöÀç ·¹º§Àº "+user_c.getString(1)+"ÀÔ´Ï´Ù");

		Total = user_c.getInt(2);
		Correct = user_c.getInt(4);
		Wrong = user_c.getInt(3);
		perc = (Correct/Total)*100;
		Percent.setText("ÇöÀç Á¤´ä·üÀº "+(int)(perc)+"% ÀÔ´Ï´Ù");
		user_c.close();

		rec_c=db.query("recent", null, null, null, null, null, null);
		startManagingCursor(rec_c);
		rec_c.moveToFirst();
		for(int l=1; l<=20 ; l++){
			rec_c.moveToPosition(l-1);
			if(rec_c.getInt(1)!=0){
				recent[l-1]=(rec_c.getInt(1))-1;
			}
			Log.i("recent[]", "½ÇÁ¦ Çà="+rec_c.getInt(0)+"  "+rec_c.getInt(1)+"  "+recent[l-1]);
		}

		rec_c.close();



		cursor=db.query(TABLE_NAME, WORD, null, null, null, null, null, null);
		startManagingCursor(cursor);
		cursor.moveToFirst();

		for(int l=1; l<=20; l++){

			if(recent[l-1]!=0){
				cursor.moveToPosition(recent[l-1]);
				RCT[l-1]=""+cursor.getString(1);
				RCT2[l-1]=""+cursor.getString(3);

			}
			Log.i("RCT[]", ""+RCT[l-1]);
		}
		cursor.close();
		cursor=db.query(TABLE_NAME, WORD, null, null, null, null, "Wrong desc", "20");
		startManagingCursor(cursor);
		cursor.moveToFirst();
		for(int l=1; l<=20; l++){
			Log.i("FQ", ""+cursor.getString(1)+"  "+cursor.getInt(7));
			if(cursor.getInt(7)!=0){
				FQ_Wrong[l-1]=""+cursor.getString(1);
				FQ_Wrong2[l-1]=""+cursor.getString(3);

				cursor.moveToNext();
			}
		}
		cursor.close();

		db.close();







		ListView list = (ListView)findViewById(R.id.id_mypage1);
		ListView list2 = (ListView)findViewById(R.id.id_mypage2);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.stglist, RCT);
		ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.stglist, FQ_Wrong);

		list.setAdapter(adapter);
		list2.setAdapter(adapter2);

		list.setOnItemClickListener(new OnItemClickListener(){

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if(RCT2[position]!="0"){
					customToastShow2(RCT[position]+"ÀÇ ¶æÀº\n"+RCT2[position]+"\nÀÔ´Ï´Ù");
				}
			}
		});

		list2.setOnItemClickListener(new OnItemClickListener(){

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if(FQ_Wrong2[position]!="0"){
					customToastShow2(FQ_Wrong[position]+"ÀÇ ¶æÀº\n"+FQ_Wrong2[position]+"\nÀÔ´Ï´Ù");
				}
			}
		});


		//		item_id = getIntent().getLongExtra("item_id", item_id);
		//		item_id++;
		//
		//		String Lv = "STG = 1."+item_id;


		/*
		// Ç¥½ÃÇÒ ¼öÄ¡°ª  
		List<double[]> values = new ArrayList<double[]>();  
		values.add(new double[] { 14230, 12300, 14240, 15244, 15900, 19200,  
				22030, 21200, 19500, 15500 });  

		//±×·¡ÇÁ Ãâ·ÂÀ» À§ÇÑ ±×·¡ÇÈ ¼Ó¼º ÁöÁ¤°´Ã¼   
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();  

		// »ó´Ü Ç¥½Ã Á¦¸ñ°ú ±ÛÀÚ Å©±â  
		renderer.setChartTitle("2011³âµµ ÆÇ¸Å·®");  
		renderer.setChartTitleTextSize(20);  

		// ºÐ·ù¿¡ ´ëÇÑ ÀÌ¸§  
		String[] titles = new String[] { "¿ùº° ÆÇ¸Å·®" };  

		// Ç×¸ñÀ» Ç¥½ÃÇÏ´Âµ¥ »ç¿ëµÉ »ö»ó°ª  
		int[] colors = new int[] { Color.YELLOW };  

		// ºÐ·ù¸í ±ÛÀÚ Å©±â ¹× °¢ »ö»ó ÁöÁ¤  
		renderer.setLegendTextSize(15);  
		int length = colors.length;  
		for (int i = 0; i < length; i++) {  
			SimpleSeriesRenderer r = new SimpleSeriesRenderer();  
			r.setColor(colors[i]);  
			renderer.addSeriesRenderer(r);  
		}  

		// X,YÃà Ç×¸ñÀÌ¸§°ú ±ÛÀÚ Å©±â  
		renderer.setXTitle("¿ù");  
		renderer.setYTitle("ÆÇ¸Å·®");  
		renderer.setAxisTitleTextSize(12);  

		// ¼öÄ¡°ª ±ÛÀÚ Å©±â / XÃà ÃÖ¼Ò,ÃÖ´ë°ª / YÃà ÃÖ¼Ò,ÃÖ´ë°ª  
		renderer.setLabelsTextSize(10);  
		renderer.setXAxisMin(0.5);  
		renderer.setXAxisMax(10.5);  
		renderer.setYAxisMin(0);  
		renderer.setYAxisMax(24000);  

		// X,YÃà ¶óÀÎ »ö»ó  
		renderer.setAxesColor(Color.WHITE);  
		// »ó´ÜÁ¦¸ñ, X,YÃà Á¦¸ñ, ¼öÄ¡°ªÀÇ ±ÛÀÚ »ö»ó  
		renderer.setLabelsColor(Color.CYAN);  

		// XÃàÀÇ Ç¥½Ã °£°Ý  
		renderer.setXLabels(2);  
		// YÃàÀÇ Ç¥½Ã °£°Ý  
		renderer.setYLabels(2);  

		// X,YÃà Á¤·Ä¹æÇâ  
		renderer.setXLabelsAlign(Align.CENTER);  
		renderer.setYLabelsAlign(Align.CENTER);  
		// X,YÃà ½ºÅ©·Ñ ¿©ºÎ ON/OFF  
		renderer.setPanEnabled(false, false);  
		// ZOOM±â´É ON/OFF  
		renderer.setZoomEnabled(false, false);  
		// ZOOM ºñÀ²  
		renderer.setZoomRate(1.0f);  
		// ¸·´ë°£ °£°Ý  
		renderer.setBarSpacing(0.08f);  

		// ¼³Á¤ Á¤º¸ ¼³Á¤  
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();  
		for (int i = 0; i < titles.length; i++) {  
			CategorySeries series = new CategorySeries(titles[i]);  
			double[] v = values.get(i);  
			int seriesLength = v.length;  
			for (int k = 0; k < seriesLength; k++) {  
				series.add(v[k]);  
			}  
			dataset.addSeries(series.toXYSeries());  
		}  

		// ±×·¡ÇÁ °´Ã¼ »ý¼º  
		GraphicalView gv = ChartFactory.getBarChartView(this, dataset,  
				renderer, Type.STACKED);  

		// ±×·¡ÇÁ¸¦ LinearLayout¿¡ Ãß°¡  
		LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.linearLayout1);  
		linearLayout1.addView(gv);  
	}  
		 */


	}	

	//Ä¿½ºÅÍ¸¶ÀÌÂ¡µÈ Åä½ºÆ®¸¦ ¶ç¿ì´Â ºÎºÐ
	public void customToastShow(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE ±×¸²
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_SHORT); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}

	public void customToastShow2(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE ±×¸²
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_LONG); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}

}


