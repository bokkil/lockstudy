package lockstudy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;



public class ScreenOffBoradcastReceiver extends BroadcastReceiver{


	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		TelephonyManager tManager = (TelephonyManager) context.getSystemService (Context.TELEPHONY_SERVICE); 
		tManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);


		Intent i= new Intent(context, NewLockStudyActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//i.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
		Log.i("screenoff", "receive!!!");
		tManager.getCallState();
		Log.d("TAG","callstate : " + tManager.getCallState() );

		if(tManager.getCallState()==0){
			context.startActivity(i);
		}
		String action = intent.getAction();
		Log.d("TAG","action : " + action);


	}

	private PhoneStateListener phoneStateListener = new PhoneStateListener();

		public void onCallStateChanged(int state, String incomingNumber) {// 전화 수신 반응.
			// 착신 전화 번호를 받는다.
			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE :  
				break; // 폰이 울리거나 통화중이 아님.
			case TelephonyManager.CALL_STATE_RINGING :
				break; // 폰이 울린다.
			case TelephonyManager.CALL_STATE_OFFHOOK :
				break; // 폰이 현재 통화 중.
			default:      
				break;
			}

		}
	
}
