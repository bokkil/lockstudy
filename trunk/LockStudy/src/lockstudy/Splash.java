package lockstudy;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import bokkil.lockstudy.R;

public class Splash extends Activity {

	public Cursor user_c, c, c2; // 디비커서 
	public SQLiteDatabase db, db2;

	String[] WORD ={"_id", "word", "ENG", "KOR", "AtoZ", "STG", "Correct", "Wrong", "LV"};
	String usrdataupdate, serviceupdate;

	public static final String ROOT_DIR = "/data/data/bokkil.lockstudy/databases/";
	WordDBHelper mHelper;
	WordDBHelper2 upHelper;

	public int LastWords;

	int mState;
	int total;
	int i;
	int lWords2;
	final static int STATE_DONE = 0;
	final static int STATE_RUNNING = 1;
	final static int PROGRESS_DIALOG = 1;

	ProgressDialog pDialog;

	private Handler handler = new Handler();


	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER,
				WindowManager.LayoutParams.FLAG_SHOW_WALLPAPER);
		setContentView(R.layout.splash);
		mHelper= new WordDBHelper(this);
		upHelper= new WordDBHelper2(this);

		Log.i("splash", "   splash  ");



		showDialog(PROGRESS_DIALOG);
		//		Handler h = new Handler();
		//		h.postDelayed(new Runnable() {
		//			public void run() {
		//				finish();
		//			}
		//
		//		}, 500);
	}

	public void upDB(){

		try {
			Thread.sleep(1000);
		} catch (Exception e) {

		}
		db2=upHelper.getWritableDatabase();
		db=mHelper.getWritableDatabase();
		c2=db2.query("words2k", WORD, null, null, null, null, null);
		startManagingCursor(c2);
		c2.moveToFirst();
		Log.i("db_trans", c2.getString(1)+" "+c2.getString(2)+" "+c2.getString(3)+" "+c2.getString(4)+" "+c2.getInt(5)+" "+c2.getInt(6) );

		for(i=0;i<1000;i++){
			// 필요한 데이터를 입력합니다.
			ContentValues newValues = new ContentValues();
			LastWords = c2.getInt(0);
			newValues.put("word", c2.getString(1));
			newValues.put("ENG", c2.getString(2));
			newValues.put("KOR", c2.getString(3));
			newValues.put("AtoZ", c2.getString(4));
			newValues.put("STG", c2.getInt(5));
			// 레코드를 추가합니다.
			db.insert("words", null, newValues);
			c2.moveToNext();
			newValues.clear();
		}

		serviceupdate="UPDATE userdata SET service_on=0";
		db.execSQL(serviceupdate);
		c2.close();
		db2.close();
		db.close();
		//Log.i("ser_on???", "     "+service_On);

	}

	public void customToastShow(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE 그림
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_SHORT); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}

	public void customToastShow2(CharSequence text) {
		TextView tv = new TextView(this.getApplicationContext()); 
		tv.setText(text);
		tv.setTextSize(20);
		tv.setTextColor(Color.WHITE);
		tv.setPadding(20, 20, 20, 20);


		LinearLayout ll = new LinearLayout(this.getApplicationContext()); 
		ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)); 
		//NOTICE 그림
		ll.setBackgroundResource(R.drawable.shape);
		ll.setGravity(Gravity.CENTER); 
		ll.addView(tv); 
		Toast t = Toast.makeText(this.getApplicationContext(), "", Toast.LENGTH_LONG); 
		t.setGravity(Gravity.CENTER, 0, 0); 
		t.setView(ll); 
		t.show(); 
	}

	public class WordDBHelper2 extends SQLiteOpenHelper{
		private static final int DATABASE_VERSION = 1;

		public WordDBHelper2(Context context) {
			super(context, "voca2.db", null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub

		}

		@Override
		public void onCreate(SQLiteDatabase db2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
		}
	}

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case PROGRESS_DIALOG:
			pDialog = new ProgressDialog(Splash.this);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setMessage("Loading...");
			pDialog.setCancelable(false);
			pDialog.setMax(2000);
			//pDialog.show();

			Thread thread = new Thread(null, doBackgroundThreadProcessing, "Background");
			thread.start();
			return pDialog;
		default:
			return null;
		}
	}



	// 백그라운드 처리 메서드를 실행하는 Runnable
	private Runnable doBackgroundThreadProcessing = new Runnable(){
		public void run() {
			Log.i("splash", "   backgroundprocessing  ");
			try {
				Thread.sleep(2000);
			} catch (Exception e) {

			}

			backgroundThreadProcessing();

		}
	};

	// 백그라운드에서 몇 가지 처리를 수행하는 메서드
	private void backgroundThreadProcessing(){
		//[필요한 코드] 
		// 처리가 끝나고 결과를 UI로 출력을 해야 할 때 아래 핸들러를 추가해서 사용한다.

		handler.post(doUpdateGUI);
		upDB();

	}

	// GUI 업데이트 메서드를 실행하는 Runnable.
	private Runnable doUpdateGUI = new Runnable(){
		public void run() {
			mState = STATE_RUNNING;
			try {
				Thread.sleep(1000);
			} catch (Exception e) {

			}
			while (mState == STATE_RUNNING) {
				try {
					Thread.sleep(1000);
				} catch (Exception e) {

				}
				Log.i("last", "      "+LastWords+"  ");
				//pDialog.setProgress(LastWords);
				Message msg = mhandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("lWords", LastWords);
				msg.setData(b);
				Log.i("hh", "      "+msg.getData().getInt("lWords"));
				mhandler.sendMessage(msg);
				if(LastWords >=2000){
					mState = STATE_DONE;
					pDialog.dismiss();
					updateGUI();

				}
			}
		}

		private void updateGUI() {
			// [[ 필요한 UI 코드 ]] 
			customToastShow("update complete!!");
			dismissDialog(PROGRESS_DIALOG);
			removeDialog(PROGRESS_DIALOG);
			finish();
		}
	};

	final Handler mhandler = new Handler() {
		public void handleMessage(Message msg) {
			lWords2 = msg.getData().getInt("lWords");
			Log.i("hhh", "     handler  ");
			pDialog.setProgress(lWords2);
		}
	};
}


