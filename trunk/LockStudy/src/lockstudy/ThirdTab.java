package lockstudy;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import bokkil.lockstudy.R;

public class ThirdTab extends Activity {
	public SQLiteDatabase db; //디비 
	public Cursor cursor; // 디비커서 
	WordDBHelper mHelper; //디비아답터 
	public SimpleCursorAdapter Adapter = null;
	public static final String TABLE_NAME ="words";
	String[] WORD ={"_id", "word"};
	String Lv;
	long item_id;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.thirdt);

		item_id = getIntent().getLongExtra("item_id", item_id);
		item_id++;

		String Lv = "STG = 1."+item_id;

		mHelper=new WordDBHelper(this);
		db=mHelper.getWritableDatabase();
		SQLiteDatabase db = mHelper.getWritableDatabase();
		cursor=db.query(TABLE_NAME, WORD, null, null, null, null, null, null);
		startManagingCursor(cursor);

		ListView list = (ListView)findViewById(R.id.word_list);


		Adapter = new SimpleCursorAdapter(list.getContext(),
				android.R.layout.simple_list_item_1, cursor,
				new String[] {"word"}, new int[] {android.R.id.text1});

		list.setAdapter(Adapter);




	}
}
